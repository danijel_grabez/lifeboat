
module.exports = function(grunt) {

  // Measures the time each task takes.
  // Does not work with grunt watch task, only separate task(s).
  require('time-grunt')(grunt);

  // Grunt config plugin enables splitting tasks into smaller chunks
  // which are placed inside grunt folder.
  require('load-grunt-config')(grunt);

  // Load installed plugins.
  // All installed plugins which are used in this project.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-postcss');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-includes');
  grunt.loadNpmTasks('grunt-svgstore');
  grunt.loadNpmTasks('grunt-contrib-htmlmin');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-browser-sync');

  grunt.loadNpmTasks('grunt-newer');
  // Grunt-newer plugin increases task running speed.
  // Settings require only adding 'newer:' as the first argument when running other tasks
  // (see registered tasks bellow - only images for now).


  //Run Tasks in specific order
  // All task(s) - Runs with 'grunt' command.
  grunt.registerTask('default', ['includes', 'htmlmin', 'uglify', 'jshint', 'less', 'postcss', 'imagemin', 'svgstore', 'copy-assets']);
  // Build html files from html page elements.
  grunt.registerTask('compile-html', ['includes', 'htmlmin']);
  // Compile less files (first compile less into css, and then run postcss task).
  grunt.registerTask('compile-css', ['less', 'postcss']);
  // Compile main js files (first run uglify task, and then run jshint task).
  grunt.registerTask('compile-js', ['uglify:js', 'jshint']);
  // Compile base js files (fallback for older browsers). This task is already built and scripts are
  // placed inside assets/js folder; grunt-watch doesn't watch for changes which are defined within this task.
  grunt.registerTask('compile-js-base', ['uglify:jsbase', 'jshint']);
  // Optimize images.
  grunt.registerTask('optimize-images', ['newer:imagemin']);
  // Generate SVG sprites.
  grunt.registerTask('icons', ['svgstore']);
  // Copy JavaScript files to production.
  grunt.registerTask('copy-javascript', ['copy:assetsJavaScript']);
  // Copy asset files (fonts, favicons and SVG files outside of images/icons folder).
  grunt.registerTask('copy-assets', ['copy:assetsFavIcons','copy:assetsSVG','copy:assetsFonts']);



  // Main task:
  // 1. 'grunt watch-files' - watch file changes (tasks are defined in grunt/watch.js) and refresh the browser upon these changes.
  grunt.registerTask('watch-files', ['browserSync', 'watch']);

};

$(document).ready(function() {

  // ===========================================================================
  //
  // Controls for accordion
  // Source: http://inspirationalpixels.com/tutorials/creating-an-accordion-with-html-css-jquery
  //
  // Example markup:
  //
  // HTML:
  //
  // <div class="accordion">
  //   <a class="accordion__title" href="#accordion-1">Accordion Section #1</a>
  //   <a class="accordion__title" href="#accordion-2">Accordion Section #2</a>
  //   <a class="accordion__title" href="#accordion-3">Accordion Section #3</a>
  //
  //   <div id="accordion-1" class="accordion__content">
  //       <p>Section Content 1.</p>
  //   </div>
  //
  //   <div id="accordion-2" class="accordion__content">
  //       <p>Section Content 2.</p>
  //   </div>
  //
  //   <div id="accordion-3" class="accordion__content">
  //       <p>Section Content 3.</p>
  //   </div>
  // </div>

  function closeAccordionSection() {
    $('.accordion__title').removeClass('is-active');
    $('.accordion__content').slideUp(400).removeClass('is-open');
  }

  $('.accordion__title').click(function(e) {
    // Grab current anchor value
    var currentAttrValue = $(this).attr('href');

    if($(e.target).is('.is-active')) {
      closeAccordionSection();
    }else {
      closeAccordionSection();

      // Add is-active class to section title
      $(this).addClass('is-active');
      // Open up the hidden content panel
      $('.accordion ' + currentAttrValue).slideDown(400).addClass('is-open');
    }

    e.preventDefault();
  });

}); // end document ready

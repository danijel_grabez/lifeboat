$(document).ready(function() {

  // ===========================================================================
  //
  // Google Map API
  // https://developers.google.com/maps/documentation/javascript/examples/#signed-in-maps
  //
  // Example markup:
  //
  // <div id="map-container" class="map-container"></div>
  //
  // .map-container {
  //   width: 100%;
  //   height: 300px;
  // }

  function initMap() {
    var mapOptions = {
      zoom: 18,
      scrollwheel: false,
      center: new google.maps.LatLng(45.2731505,19.7746956)
    };
    var map = new google.maps.Map(document.getElementById('map-container'), mapOptions);
    var myLatlng = new google.maps.LatLng(45.2731505,19.7746956);

    var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
    });

  }

  function loadScript() {
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDenSA2SJqgybFGFkzrNxUMPOXwiIeWuTs&callback=initMap';
    document.body.appendChild(script);
  }
  window.onload = loadScript;

}); // end document ready

$(document).ready(function() {

  // ===========================================================================
  //
  // Make table row link
  //
  // Example markup:
  //
  // <tr class='is-link' data-href='url://'>

  $('.is-link').click(function() {
    window.document.location = $(this).data('href');
  });


}); // end document ready

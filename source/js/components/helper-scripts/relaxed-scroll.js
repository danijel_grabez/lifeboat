$(document).ready(function() {

  // ===========================================================================
  //
  // Make scrolling comfortable
  //
  // Add `.js-relaxed-scroll` on element which is scrollable.

  $( '.js-relaxed-scroll' ).bind( 'mousewheel DOMMouseScroll', function ( e ) {
    var e0 = e.originalEvent,
      delta = e0.wheelDelta || -e0.detail;
    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 15;
    e.preventDefault();
  });

}); // end document ready

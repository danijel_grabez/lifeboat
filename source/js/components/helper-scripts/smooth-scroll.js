$(document).ready(function() {

  // ===========================================================================
  //
  // Smooth scroll-to links
  // Originally from http://stackoverflow.com/a/7717572/764886
  //
  // Example markup:
  //
  // <a href="#anchor" class="js-scroll-to">I will scroll</a>

  $('.js-scroll-to').click(function(e){
    $('html, body').animate({
      scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 500);
    e.preventDefault();
  });

}); // end document ready

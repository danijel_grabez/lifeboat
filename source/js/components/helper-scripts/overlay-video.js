$(document).ready(function() {

  // ===========================================================================
  //
  // Play video after opening overlay and stop it after closing
  // (this segment is in connection with overlay.js script)
  //
  // Example markup:
  //
  // HTML:
  // Put iframe element inside .overlay-content element
  //
  // <iframe class="js-intro-video" src="https://player.vimeo.com/video/VIDEO_URL_HERE?title=0&byline=0&portrait=0" width="800" height="450" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  //
  // <a href="#overlay-id" class="js-overlay-toggle js-play-video">Play video in overlay</a>

  var $playVideoAction = $('.js-play-video');
  var $stopVideoAction = $('.js-overlay-close');
  var $video = '';
  var src = '';

  // Play video
  $playVideoAction.on('click', function (e) {
    var videoContainer = $(e.target).attr('href');
    $video = $(videoContainer).find('iframe[src]');
    if ($video.length > 0) {
      src = $video.attr('src');
      var srcPlay =  src.indexOf('?') > -1 ? src + '&autoplay=1' : src + '?autoplay=1';
      $video.attr('src', '');
      $video.attr('src', srcPlay);
    }
  });

  // Stop video
  $stopVideoAction.on('click', function (e) {
    e.stopPropagation();
    $video.attr('src', src);
  });

}); // end document ready

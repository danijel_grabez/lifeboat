$(document).ready(function() {

  // ===========================================================================
  //
  // Show menu navigation on smaller screen resolutions

  var menu = $('.page-nav__list');

  $('#page-nav-button').click(function() {
    menu.slideToggle(500);
  });

  $(window).on('resize', function(){
    if(!jQuery('#page-nav-button').is(':visible') && !menu.is(':visible')) {
      menu.css({'display':''});
    }
  });

}); // end document ready

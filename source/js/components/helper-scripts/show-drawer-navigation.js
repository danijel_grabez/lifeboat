$(document).ready(function() {

  // ===========================================================================
  //
  // Show alternate menu navigation on smaller screen resolutions -- Drawer effect

  var viewport = $(window),
    page = $('body');

  $('#page-nav-button').click(function() {
    page.toggleClass('js-push-page');
  });

  $(window).on('resize', function() {
    if (viewport.width() > 990) {
      page.removeClass('js-push-page');
    }
  });

}); // end document ready

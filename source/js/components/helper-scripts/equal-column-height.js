$(document).ready(function() {

  // ===========================================================================
  //
  // Equal height containers (for e.g. equal height columns, or rows)
  //
  // Example markup:
  //
  // HTML:
  // <div class="js-element-equal-height">
  //   Element content 1
  // </div>
  // <div class="js-element-equal-height">
  //   Element content 2
  // </div>
  // <div class="js-element-equal-height">
  //   Element content 3
  // </div>

  var $equalHeight = $('.js-element-equal-height');
  var height = 0;
  $equalHeight.each(function () {
    if ($(this).height() > height) {
      height = $(this).height();
    }
  });
  $equalHeight.height(height);

}); // end document ready

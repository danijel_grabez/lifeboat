$(document).ready(function() {

  // ===========================================================================
  //
  // Display only certain ammount of elements from the list

  var displayPartially = 4;
  $('.piece-list .piece').slice(displayPartially).toggle();
  $('.show-summary-count').click(function(e){
    e.preventDefault();
    // show whole list
    $('.piece-list .piece').not($('.piece').slice(0,4)).fadeToggle();
  });

}); // end document ready

$(document).ready(function() {

  // ===========================================================================
  //
  // Controls for a tabbed interface which remembers #id in address bar.
  // Source: https://24ways.org/2015/how-tabs-should-work/
  //
  // Example markup:
  //
  // HTML:
  //
  //  <div class="tab-environment">
  //    <nav class="tab-nav">
  //      <ul role="tablist" class="tab-nav__list">
  //        <li class="tab-nav__item">
  //          <a href="#tab-1" role="tab" aria-controls="tab-1" class="tab-nav__link">Tab 1</a>
  //        </li>
  //        <li class="tab-nav__item">
  //          <a href="#tab-2" role="tab" aria-controls="tab-2" class="tab-nav__link">Tab 2</a>
  //        </li>
  //        <li class="tab-nav__item">
  //          <a href="#tab-3" role="tab" aria-controls="tab-3" class="tab-nav__link">Tab 3</a>
  //        </li>
  //      </ul
  //    </nav>
  //
  //    <div class="tab-group">
  //      <div role="tabpanel" class="tab-item" id="tab-1">
  //        <p>Tab #1 content goes here!</p>
  //      </div>
  //      <div role="tabpanel" class="tab-item" id="tab-2">
  //        <p>Tab #2 content goes here!</p>
  //      </div>
  //      <div role="tabpanel" class="tab-item" id="tab-3">
  //        <p>Tab #3 content goes here!</p>
  //      </div>
  //    </div>
  //
  //  </div>

  // A temp value to cache *what* we're about to show
  var target = null;

  // collect all the tabs
  var tabs = $('.tab-nav__link').on('click', function () {
    console.log('click');
    target = $(this.hash).removeAttr('id');
    if (location.hash === this.hash) {
      setTimeout(update);
    }
  }).attr('tabindex', '0');

  // get an array of the panel ids (from the anchor hash)
  var targets = tabs.map(function () {
    return this.hash;
  }).get();

  // use those ids to get a jQuery collection of panels
  var panels = $(targets.join(',')).each(function () {
    // keep a copy of what the original el.id was
    $(this).data('old-id', this.id);
  });

  function update() {
    console.log('update');
    if (target) {
      target.attr('id', target.data('old-id'));
      target = null;
    }

    var hash = window.location.hash;
    if (targets.indexOf(hash) !== -1) {
      return show(hash);
    }

    // fix going "back" on the browser nav to an empty state
    if (!hash) {
      show();
    }
  }

  function show(id) {
    // if no value was given, let's take the first panel
    if (!id) {
      id = targets[0];
    }
    // remove the selected class from the tabs,
    // and add it back to the one the user selected
    tabs.removeClass('is-active').attr('aria-selected', 'false').filter(function () {
      return (this.hash === id);
    }).addClass('is-active').attr('aria-selected', 'true');

    // now hide all the panels, then filter to
    // the one we're interested in, and show it
    panels.hide().attr('aria-hidden', 'true').filter(id).fadeIn().attr('aria-hidden', 'false');
  }

  window.addEventListener('hashchange', update);

  // initialise
  if (targets.indexOf(window.location.hash) !== -1) {
    update();
  } else {
    show();
  }

}); // end document ready

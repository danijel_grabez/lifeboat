$(document).ready(function() {

  // ===========================================================================
  //
  // Localstorage for input elements (store entered form data to localstorage memory)
  //
  // Example markup:
  //
  // <form action="" class="form">
  //   <div class="holder">
  //     <label for="name" class="form__label">Your name</label>
  //     <input type="text" id="name" name="name" placeholder="Your first name" class="form__input">
  //   </div>
  //   <div class="holder">
  //     <label for="email" class="form__label">Your email</label>
  //     <input type="text" id="email" name="email" placeholder="Your last name" class="form__input">
  //   </div>
  //   <div class="holder">
  //     <label for="option" class="form__label">Your option</label>
  //     <select name="select-option" id="select-option" class="select">
  //         <option value="select-option-01">Option 1</option>
  //         <option value="select-option-02">Option 2</option>
  //         <option value="select-option-03">Option 3</option>
  //     </select>
  //   </div>
  //   <div class="holder">
  //     <label>
  //       <input type="radio" id="radio-option-01" name=" radio-option" value="radio-option-01">Yes</label>
  //     <label>
  //       <input type="radio" id="radio-option-02" name="radio-option" value="radio-option-02">No</label>
  //     <label>
  //       <input type="radio" id="radio-option-03" name="radio-option" value="radio-option-03">Maybe</label>
  //   </div>
  //   <div class="holder">
  //     <label for="textarea" class="form__label">Your message</label>
  //     <textarea name="textarea" id="textarea"></textarea>
  //   <div class="holder">
  //     <input type="submit">
  //   </div>
  // </form>

  // Check if browser support localstorage
  if(typeof(Storage) !== 'undefined') {

    // Prevent autocomplete
    // (localstorage doesn't function well with autocomplete enabled)
    $( document ).on( 'focus', ':input', function(){
      $( this ).attr( 'autocomplete', 'off' );
    });

    // Localstorage for input[type="text"] and textarea elements
    $('.form input[type="text"], .form textarea').each(function(){
      if ( localStorage[$(this).attr('name')] ) {
        $(this).val( localStorage[$(this).attr('name')] );
      }
    });

    $('.form input[type="text"], .form textarea').keyup(function(){
      localStorage[$(this).attr('name')] = $(this).val();
    });

    // Localstorage for input[type="radio"] element
    $('.form input[type="radio"]').each(function(){
      var radioName = $(this).attr('name');
      var content = localStorage.getItem(radioName);

      if(content !== null) {
        $('input[name="'+radioName+'"]').each(function(){
        //...check each button...
          if($(this).val() === content) {
          //...and if the value of "content" (referenced above) matches...
            $(this).attr('checked','checked');
            //...check this radio button.
          }
        });
      }

      $(this).bind('click',function(){
        localStorage[radioName] = $(this).val();
        //store the value of the button whenever one is clicked
      });

    });

    // Localstorage for select element
    $('.form select').each(function(){
      if ( localStorage[$(this).attr('name')] ) {
        $(this).val( localStorage[$(this).attr('name')] );
      }
    });

    $('.form select').change(function(){
      localStorage[$(this).attr('name')] = $(this).val();
    });

    // Clear localstorage on form submit
    $('.form').submit(function() {
      localStorage.clear();
    });

  }

}); // end document ready

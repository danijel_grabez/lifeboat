$(document).ready(function() {
	
	// ===========================================================================
  //
  // Defer images 
  // 
  // Example markup:
  // 
  // Images with data-src attribute needs to hold the path to the image, while
  // src attribute holds placeholder image – base64 version. Script for image 
  // deffering should be placed inside each HTML page.
  // 
  // <img src="data:image/png;base64,R0lGODlhAQABAAD/ACwAAAAAAQABAAACADs=" data-src="images/image-name.jpg"/>

  // Defer images
  // script(type='text/javascript').
  //   function init() {
  //     var imgDefer = document.getElementsByTagName('img');
  //     for (var i=0; i<imgDefer.length; i++) {
  //       if(imgDefer[i].getAttribute('data-src')) {
  //         imgDefer[i].setAttribute('src',imgDefer[i].getAttribute('data-src'));
  //         $('img').css('opacity', '1');
  //       }
  //     }
  //   }
  //   window.onload = init;

}); // end document ready
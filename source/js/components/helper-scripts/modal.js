$(document).ready(function() {

  // ===========================================================================
  //
  // Modal
  //
  // Example markup:
  //
  // HTML:
  // <div id="modal-id" class="modal" role="alert">
  //   <div class="modal__container">
  //     <header class="modal__header">
  //       <h4>Modal header content</h4>
  //     </header>
  //     <div class="modal__content">
  //       <span>Modal content</span>
  //     </div>
  //     <footer class="modal__footer">
  //       <span>Modal footer content</span>
  //     </footer>
  //     <a href="#modal-id" class="modal__close js-modal-close">
  //       <svg class="icon">
  //         <use xlink:href="#icon-close" xmlns:xlink="http://www.w3.org/1999/xlink"/>
  //       </svg>
  //     </a>
  //   </div>
  // </div>
  //
  // <a href="#modal-id" class="js-modal-trigger">Trigger Modal 1</a>

  // Open modal
  $('.js-modal-trigger').on('click', function(event){
    event.preventDefault();
    $('.modal').filter(this.hash).addClass('is-visible');
    $('body').addClass('js-modal-active');
  });

  // Close modal
  $('.modal').on('click', function(event){
    if( $(event.target).is('.js-modal-close') || $(event.target).is('.modal') ) {
      event.preventDefault();
      $(this).removeClass('is-visible');
      $('body').removeClass('js-modal-active');
    }
  });

  // Close modal when clicking the esc keyboard button
  $(document).keyup(function(event){
    if(event.which=='27'){
      $('.modal').removeClass('is-visible');
      $('body').removeClass('js-modal-active');
    }
  });

}); // end document ready

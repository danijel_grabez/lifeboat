$(document).ready(function() {

  // ===========================================================================
  //
  // Controls for a tabbed interface
  // Source: http://inspirationalpixels.com/tutorials/creating-tabs-with-html-css-and-jquery
  //
  // Example markup:
  //
  // HTML:
  //
  // <div class="tab-environment">
  //   <nav class="tab-nav">
  //     <ul class="tab-nav__list">
  //       <li class="tab-nav__item"><a href="#tab1" class="tab-nav__link is-active">Tab #1</a></li>
  //       <li class="tab-nav__item"><a href="#tab2" class="tab-nav__link">Tab #2</a></li>
  //       <li class="tab-nav__item"><a href="#tab3" class="tab-nav__link">Tab #3</a></li>
  //       <li class="tab-nav__item"><a href="#tab4" class="tab-nav__link">Tab #4</a></li>
  //     </ul>
  //   </nav>
  //
  //   <div class="tab-group">
  //     <div id="tab1" class="tab-item is-active">
  //       <p>Tab #1 content goes here!</p>
  //     </div>
  //
  //     <div id="tab2" class="tab-item">
  //       <p>Tab #2 content goes here!</p>
  //     </div>
  //
  //     <div id="tab3" class="tab-item">
  //       <p>Tab #3 content goes here!</p>
  //     </div>
  //
  //     <div id="tab4" class="tab-item">
  //       <p>Tab #4 content goes here!</p>
  //     </div>
  //   </div>
  //
  // </div>

  $('.tab-nav__link').on('click', function(e)  {
    var currentAttrValue = $(this).attr('href');

    // Show/Hide Tabs
    $('.tab-environment ' + currentAttrValue).fadeIn(400).siblings().hide();

    // Change/remove current tab to active
    $(this).closest('.tab-nav__list').find('.tab-nav__link').removeClass('is-active');
    $(this).addClass('is-active');

    e.preventDefault();
  });

}); // end document ready

$(document).ready(function() {

  // ===========================================================================
  //
  // Dropdown
  //
  // Example markup:
  //
  // HTML:
  // <div class="dropdown-environment">
  //  <a href="#dropdown-id" class="js-dropdown-trigger dropdown-trigger">Click Me</a>
  //
  //  <div id="dropdown-id" class="dropdown">
  //    <div class="dropdown-arrow"></div>
  //    <ul class="dropdown__list">
  //      <li class="dropdown__item"><a href="#" class="dropdown__link">Link item</a></li>
  //      <li class="dropdown__item"><a href="#" class="dropdown__link">Link item</a></li>
  //      <li class="dropdown__item"><a href="#" class="dropdown__link">Link item</a></li>
  //      <li class="dropdown__item"><a href="#" class="dropdown__link">Link item</a></li>
  //    </ul>
  //  </div>
  // </div>

  $('.js-dropdown-trigger').on('click', function(e)  {
    // Show/Hide dropdown
    $('.dropdown').filter(this.hash).toggleClass('is-open');
    // Change/Remove dropdown trigger active state
    $(this).toggleClass('is-active');
    e.preventDefault();
  });

}); // end document ready

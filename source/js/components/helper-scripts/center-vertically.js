$(document).ready(function() {

  // ===========================================================================
  //
  // Center content vertically
  //
  // Example markup:
  //
  // <div class="container-class">
  //  <div class="center-content">
  //    Content which is going to be centered.
  //  </div>
  // </div>

  $(window).resize(function() {
    $('.center-content').css({
      'padding-top' : ($('.container-class').height()/2)-($('.center-content').height()/2)
    });
  });
  $(window).resize();


}); // end document ready

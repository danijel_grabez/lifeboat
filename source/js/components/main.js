$(document).ready(function() {


  // ===========================================================================
  //
  // Notification banner for IE lt 9
  var ieNotification = $('.browsehappy');
  $('.browsehappy__dismiss').click(function() {
    ieNotification.remove();
  });


  // ===========================================================================
  //
  // SVG Fallback for older browsers

  if (!Modernizr.svg) {
    $('img[src$=".svg"]').each(function() {
      //Replace 'image.svg' with 'image.png'.
      $(this).attr('src', $(this).attr('src').replace('.svg', '.png'));
    });
  }


  // ===========================================================================
  //
  // Show menu navigation on smaller screen resolutions

  var menu = $('.page-nav__list');

  $('#page-nav-button').click(function() {
    menu.slideToggle(300);
  });

  $(window).on('resize', function(){
    if(!jQuery('#page-nav-button').is(':visible') && !menu.is(':visible')) {
      menu.css({'display':''});
    }
  });


}); // end document ready

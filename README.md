# Project_name

This is a Project_name repository which is built with [Lifeboat](https://bitbucket.org/danijel_grabez/less-project-starter).

### Installation

Install required grunt-related libraries with `npm install`.  
Install dploy with `dploy install`.

### Building the assets

Run main grunt task with `grunt watch-files`.  
All assets source files are located in `/source/less, js, images` and built files are located in `/assets/css, js, images`.  
HTML pages should be created in `source/site-pages` if `grunt-includes` task is enabled. This task builds html pages in the root directory.  

**Handy tasks:**  
1. `grunt copy-assets` – copy fonts, favicons and SVG files which are not handled by `watch` task.  
2. `grunt copy-javascript` – copy all JavaScript plugins used in project. This task is handy in situations when the repo needs to be passed to developers who are integrating the files without Grunt setup.  

### Resources and tools

- [LESS](http://lesscss.org/) — CSS Preprocessor
- [NPM](https://nodejs.org/) — Node Package Manager
- [Grunt.js](http://gruntjs.com/) — JavaScript Task Runner
- [DPLOY](http://lucasmotta.github.io/dploy/) — Deployment Tool
- [Surge](https://surge.sh/) — Deploy projects to a production-quality CDN 

For more information about each of the mentioned tool see [Tools and Services Page](documentation/tools-and-services.md).

### Author
Danijel Grabež  
email: danijel889@gmail.com

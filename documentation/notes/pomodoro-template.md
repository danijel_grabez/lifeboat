# 🆕 TODO List                                 [14 •]
    ☐ Regular list item #1                     •••◦        ’’’     ---
    ☐ List item #2                             •◆          ’’      
    ⚑ Urgent list item #3                      ••          ’’      -
    ☐ List item #4                             •••◦        ’’’
    ☐ List item #5                             ◦◦          ’’      ----
    ☐ List item #6                             ◦           ’’      ----
      + Combined list item #7
      + Combined item #8

    ☐ Unplanned list item #1
    ⚑ Unplanned urgent list item #1
    ☐ Unplanned list item #3



# ❔ Questions
    ☐ List item #1
    ☐ List item #2



# 🕓 Later
    ☐ List item #1
    ☐ List item #1



# 📁 Archived
~~DD/MM/YYYY            [14 •] – № of completed pomodoros  
✔ List item #1           ••••  
✔ List item #2           ••  
✗ List item #3           •~~  

| **Date**    | **№ 🍅**        | **Type of work**
| :---------- | :--------------: | :---------------------------- |
| DD/MM/YYYY  | 14               | Description of work activity. | 
| DD/MM/YYYY  | 14               | Description of work activity. |
| DD/MM/YYYY  | 14               | Description of work activity. |
| **Total:**  | **42**           |                               |



# 📎 Additional Notes
Jot down additional information about the project.



# 💡 Pomodoro technique



# 💡 Task status icons
    To do                   ☐
    Checked                 ✔
    Unfinished              ✗
    Urgent                  ⚑
    Pomodoro                ◦
    Pomodoro done           •
    Unplanned               ◇
    Unplanned done          ◆
    Interruption internal   ’
    Interruption external   -



## 💡 Icon variations
    To do           ☐
    Checked         ✅   ☑   ✔    ☒
    Unfinished      ✖   ❌   ❎   ⛔   ✕   ✗   ✘
    Urgent          ⭐   🔥    ⚡   📌   📍   ⚐   ⚑
    
    New             🆕
    Later           🕓   📅   📆
    Archived        📁   📂   👏   👍   👌
    Question        ❓   ❔   💬
    Info            ✎    📎   ℹ  👉   💡
    Other           📄   ❤   ✳   ⚛   *   
    Pomodoro        •   ◦   ◇   ◆   ’   - 

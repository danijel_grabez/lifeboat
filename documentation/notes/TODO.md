# 🆕 TODO List
    ☐ List item #1
    ☐ List item #2
    ⚑ Urgent list item #3
    ☐ List item #4
    ☐ List item #5
    ☐ List item #6
      ☐ Sub-list item #1
      ☐ Sub-list item #2



# ❔ Questions
    ☐ List item #1
    ☐ List item #2



# 🕓 Later
    ☐ List item #1
    ☐ List item #1



# 📁 Archived
~~DD/MM/YYYY            [14 •]
✔ List item #1
✔ List item #2
✗ List item #3~~



# 📎 Additional Notes
Jot down additional information about the project.



# 💡 About .note documents
This file is created with Plain Notes plugin for Sublime Text 3.
In order to enable saving notes inside /documentation/notes/ folder you will need to change the preferences by adding the following in your **.sublime-project** file:

    "settings":
        {
            "PlainNotes": 
            {
                "root": "path/to/project/documentation/notes/"
            }
        }


# 💡 Task status icons
    To do           ☐
    Checked         ✔
    Unfinished      ✗
    Urgent          ⚑
    
    ¯\_(ツ)_/¯

// grunt-contrib-jshint task look for possible errors in the javascript files.
// jshint-stylish provide prettier console output.

module.exports = {

  all: 'source/js/components/**/*.js',
  options: {
    reporter: require('jshint-stylish'),
    eqeqeq: false,                               // Prohibit the use of == and != in favor of === and !==
    curly: true                                  // Require you to always put curly braces around blocks in loops and conditionals.
  }

};

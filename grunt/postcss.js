// grunt-postcss task run autoprefixing for compiled css files.
// This task depends on autoprefixer-core which provide css prefixes based on defined browser support.

module.exports = {

  options: {
    processors: [
      require('autoprefixer-core')({browsers: '> 1%'}),    // autoprefixer core represents dependency package.
    ]
  },
  dist: {
    src: 'assets/css/*.css'                                // destination of css files which will be prefixed.
  }
};

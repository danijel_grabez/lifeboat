// grunt-contrib-less compile less files into css files.

module.exports = {

  development: {                      // Compile unminified css.
    options: {
      banner: '/* Author: Danijel Grabež */\n' // Banner will be added at the top of the compiled file.
    },
    files: {                          // Dictionary of files (destination : start location).
      'assets/css/style.css': 'source/less/style.less'
    }
  },

  deploy: {                            // Compile minified css.
    options: {
      compress: true,                  // Css output style.
      banner: '/* Author: Danijel Grabež */\n' // Banner will be added at the top of the compiled file.
    },
    files: {                           // Dictionary of files (destination : start location).
      'assets/css/style.min.css': 'source/less/style.less'
    }
  }

};

// Two less tasks are defined:
// Development task creates unminified css file.
// Deploy task creates minified css file.

// grunt-svgstore task create svg sprite from source/images/icons folder.

module.exports = {

  icons: {
    files: {
      'assets/images/icons/icons.svg': ['source/images/icons/*.svg']
    },
    options: {
      prefix: 'icon-',                      // add prefix to all icons with an unambiguous label

      cleanup: true,                        // cleans fill, stroke, stroke-width attributes
                                            // so that we can style them from the css.

      convertNameToId: function(name) {     // write a custom function to strip the first part
        return name.replace(/^\w+\_/, '');  // of the file that Adobe Illustrator generates
      }                                     // when exporting the artboards to SVG.
    }
  }

};

// Content from svg file needs to be added inside source/site-pages/partials/svg-holder.html
// This partial needs to be added to every page which can be time consuming.
// In order to enable caching of inline svg file local storage is used.
//
// Grunt task named `includes` generates svg-holder.html file inside assets/images/icons/svg-holder.html.
// Local storage script injects and cache this file.

// grunt-contrib-imagemin optimize images for the web
// (.png, .jpeg, .svg, .gif are possible to be optimized).

module.exports = {

  png: {
    options: {
      optimizationLevel: 3             // Compression level.
    },
    files: [{
      expand: true,                    // Dynamic expansion.
      cwd: 'source/images/',
      src: ['**/*.png'],
      dest: 'assets/images',
      ext: '.png'
    }]
  },
  jpg: {
    options: {
      progressive: true                // Lossless or progressive conversion.
    },
    files: [{
      expand: true,
      cwd: 'source/images/',
      src: ['**/*.jpg'],
      dest: 'assets/images',
      ext: '.jpg'
    }]
  }

};

// grunt-contrib-htmlmin will minify html files from root directory

module.exports = {

  htmlmin: {
    options: {                                 // Target options
      removeComments: true,
      collapseWhitespace: true
    },
    files: [{
      expand: true,
      cwd: '.',                                // Starting location
      src: '*.html',                           // All html files will be minified
      dest: '.'                                // Destination of minified html files
    }]
  }

};

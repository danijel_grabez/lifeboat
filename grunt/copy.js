// grunt-contrib-copy
//
// grunt-contrib-copy task copy files from one location to the another.

module.exports = {

  assetsJavaScript: {                       // Copy all JavaScript plugins used in project to assets/js/unminified-scripts folder.
    files: [{                               // This task is handy in situations when the repo needs to be passed to developers
      expand: true,                         // who are integrating the files without Grunt setup.
      cwd: 'source/js',
      src: ['**', '!**vendor/ie/**', '!**vendor/jquery/**', '!**vendor/modernizr/**', '!**components/helper-scripts/**'],
      dest: 'assets/js/unminified-scripts'
    }]
  },

  assetsFavIcons: {                         // Copy fonts, favicons and SVG files (outside of images/icons folder) to assets folder.
    files: [{
      expand: true,
      cwd: 'source/images/favicons',
      src: ['**'],
      dest: 'assets/images/favicons'
    }]
  },

  assetsSVG: {
    files: [{
      expand: true,
      cwd: 'source/images',
      src: ['*.svg', '!**icons/**'],
      dest: 'assets/images/'
    }]
  },

  assetsFonts: {
    files: [{
      expand: true,
      cwd: 'source/fonts',
      src: ['**'],
      dest: 'assets/fonts/'
    }]
  }
};

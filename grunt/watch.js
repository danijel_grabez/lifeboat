// grunt-contrib-watch task run tasks whenever watched files change.
// Livereload option enables browser refreshing when this task is run
// (livereload extension needs to be enabled on your browser).

module.exports = {

  options: {
    livereload: true                        // Show changes on browser without page refreshing.
  },

  html: {                                   // Refresh the browser when there are changes in html, css or js.
    files: ['source/site-pages/**/*.html', 'source/less/**/*.less', 'source/js/**/*.js'],
    tasks: ['compile-html'],                // This task includes html partials from source/pages/layout
    options: {                              // and then compiles html pages in root directory.
      spawn: false,
      livereload: true
    }
  },

  css: {
    files: ['source/less/**/*.less'],       // When file from source/less folder is changed, run compile-css action.
    tasks: ['compile-css'],                 // This task compiles less into css, and then run postcss task.
    options: {                              // With combined actions we have to make correct task completion order
      spawn: false                          // (compile css and then postcss for autoprefixing).
    }
  },

  scripts: {                                // When file from source/js folder is changed, run compile-js action
    files: ['source/js/**/*.js'],           // (uglify and jshint tasks combined).
    tasks: ['compile-js'],
    options: {
      spawn: false
    }
  },

  imagemin: {                               // When we add, or change image from source/images folder, imagemin task is run.
    files: ['source/images/**/*.jpg', 'source/images/**/*.png'],
    tasks: ['optimize-images']
  },

  svgstore: {                               // When svg file is added, or modified from the source/images/icons folder
    files: ['source/images/icons/*.svg'],   // run svgstore task which will create new svg sprite in root/images/icons folder.
    tasks: ['svgstore']
  }

};

// grunt-watch action triggers with 'grunt watch-files' which listen file changes and triggers
// certain action (in this case we are listening to the html, css, javascript, image and svg files).

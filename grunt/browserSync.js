// grunt-browser-sync will synchronize the browser upon file changes.
// When the files are compiled by watch task, browser will refresh the page.

module.exports = {

  synchronize: {
    bsFiles: {                                // Reload the browser when these files are changed
      src : [
        '*.html',
        'assets/images/icons/svg-holder.html', // SVG icons (served via javascript to the localstorage)
        'assets/css/*.css',
        'assets/js/*.js'
      ]
    },
    options: {
      watchTask: true,                        // Synchronize with grunt-contrib-watch task
      server: './',                           // (when watch tasks compile files, browser will be reloaded)
      ui: false,                              // Don't provide browser sync user interface (that is accessed via a separate port)
      port: 9000,
      notify: false,                          // Don't show any notifications in the browser.
      logLevel: 'silent',                     // Don't show browsersync log messages
      tunnel: 'lifeboat',                     // Tunnel the Browsersync server through --> http://lifeboat.localtunnel.me. (tunnel name can't contain hyphens, or upper cases)
      online: true                            // Some features of Browsersync (such as xip & tunnel) require an internet connection,
                                              // but if you're working offline, you can reduce start-up time by setting this option to `false`.
    }
  }

};

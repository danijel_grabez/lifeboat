// grunt-includes task enable users to include html partials inside the html pages.
// (think php includes).

module.exports = {

  build: {
    cwd: 'source/site-pages/',                        // Starting location.
    src: ['*.html'],
    dest: '.',                                        // Location of compiled html files.
    options: {
      flatten: true,
      includePath: 'source/site-pages/partials/',     // Location of .html partials.
      banner: ' <!-- Grunt include -->\n'
    }
  },
  compileSvgHolder: {                                 // This task creates svg holder file which
    options: {},                                      // is used with local storage script.
    files: {
      'assets/images/icons/svg-holder.html': ['source/site-pages/partials/svg-holder.html']
    }
  }

};

// HTML pages needs to be created inside source/site-pages/.
// HTML partials needs to be created inside source/site-pages/partials/.

// Usage example:
// Add `include "page-header.html"` inside html page
// (page-header.html represents html partial).

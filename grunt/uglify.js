// grunt-contrib-uglify task concatintate, minify and uglify javascript files.

module.exports = {

  jsbase: {
    options: {
      preserveComments: 'none'                             // Remove unnecessary comments.
    },
    files: [{
      src: [                                               // Jquery fallback (in case that cdn version is not loaded).
        'source/js/vendor/jquery/jquery-1.11.2.min.js'
      ],
      dest: 'assets/js/jquery-1.11.2.min.js'
    },
    {
      src: [                                               // Modernizr (custom made).
        'source/js/vendor/modernizr/modernizr.min.js'
      ],
      dest: 'assets/js/modernizr.min.js'
    }]
  },

  js: {
    options: {
      preserveComments: 'some',                            // Keep the comments.
      banner: '/* Author: Danijel Grabež */\n'             // Banner will be added at the top of the compiled file.
    },
    files: [{
      src: [                                               // List of plugins which are used on this project, along with
        'source/js/components/main.js'                     // custom made javascript.
      ],
      dest: 'assets/js/script.min.js'
    }]
  }

};

// Two uglify tasks are defined:
// jsbase uglify task compiles support scripts for older browsers.
// js uglify task compiles plugin(s) which are used througout the project along with the custom javascript.
